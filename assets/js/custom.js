$(document).ready(function() {

    $('.dropdown-link').on('click', function() {
        $(this).closest('section').next().toggleClass('hide');
        $(this).find('.glyphicon-chevron-up').toggle();
        $(this).find('.glyphicon-chevron-down').toggle();
    });

    $(window).on('scroll', function() {
        var windowpos = $(window).scrollTop();
        var bodyHieght = $('.footer').height();
        var footerHieght = $('.stories-section').offset().top;
        if (windowpos + bodyHieght < footerHieght) {
            $('.chat').removeClass('pos-fixed');
        } else {
            $('.chat').addClass('pos-fixed');
        }
    });
});
